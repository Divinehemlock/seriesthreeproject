import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;


public class Main
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        List<String> board = new ArrayList<String>(); // making the board

        for (int i = 0 ; i < 16 ; i++) // filling the board
        {
            String temp = String.valueOf(i);

            board.add(temp);
        }

        Collections.shuffle(board); // making a random order

        int selectedIndex = 0;

        boolean isEnterPressed = false;

        String pressedKey = "";

        boardDrawer(board , selectedIndex);

        while(!isGameOver(board)) // game while , should be broken when game is over
        {
            while(true) // inputting choice
            {
                nextLine();

                System.out.println("press WASD in lower case to move or press L in lower case to lock the tile");

                pressedKey = input.nextLine();

                if (pressedKey.equals("w") || pressedKey.equals("a") || pressedKey.equals("s") || pressedKey.equals("d"))
                {
                    System.out.println("pressed key is : " + pressedKey);

                    break;
                }
                else if (pressedKey.equals("l"))
                {
                    break;
                }
                else
                {
                    System.out.println("not a lower case WASD char or l , turn off capslock or try again");
                }
            }

            switch (pressedKey) // updating selected index
            {
                case "w" : if (selectedIndex >= 4 && selectedIndex <= 16) {selectedIndex -= 4;}  break;

                case "s" : if (selectedIndex >= 0 && selectedIndex <= 11) {selectedIndex += 4;} break;

                case "a" : if (selectedIndex != 0 ) {selectedIndex -= 1;} break;

                case "d" : if (selectedIndex != 15 ) {selectedIndex += 1;} break;

                case "l" : moveFunction(selectedIndex, board);
            }

            boardDrawer(board , selectedIndex);

            nextLine();

        }

        printEndMessage();


    }

    public static void boardDrawer (List board , int selectedIndex)
    {
        horizontalLineAtTop();

        nextLine();

        for (int i = 0 ; i < 4 ; i++) // vertical line and board indices 1 - 4
        {
            System.out.print('\u2502');

            if (i == selectedIndex)
            {
                System.out.print(StringUtils.center((String) "*" + board.get(i) + "*" ,8));
            }
            else
            {
                System.out.print(StringUtils.center((String) board.get(i) ,8));
            }

            System.out.print('\u2502');
        }

        nextLine();

        horizontalLineAtMiddle();

        nextLine();

        for (int i = 4 ; i < 8 ; i++) // vertical line and board indices 5 - 8
        {
            System.out.print('\u2502');

            if (i == selectedIndex)
            {
                System.out.print(StringUtils.center((String)"*" + board.get(i) + "*" ,8));
            }
            else
            {
                System.out.print(StringUtils.center((String) board.get(i) ,8));
            }

            System.out.print('\u2502');
        }

        nextLine();

        horizontalLineAtMiddle();

        nextLine();

        for (int i = 8 ; i < 12 ; i++) // vertical line and board indices 9 - 12
        {
            System.out.print('\u2502');

            if (i == selectedIndex)
            {
                System.out.print(StringUtils.center((String)"*" + board.get(i) + "*" ,8));
            }
            else
            {
                System.out.print(StringUtils.center((String) board.get(i) ,8));
            }

            System.out.print('\u2502');
        }

        nextLine();

        horizontalLineAtMiddle();

        nextLine();

        for (int i = 12 ; i < 16 ; i++) // vertical line and board indices 13 - 16
        {
            System.out.print('\u2502');

            if (i == selectedIndex)
            {
                System.out.print(StringUtils.center((String)"*" + board.get(i) + "*" ,8));
            }
            else
            {
                System.out.print(StringUtils.center((String) board.get(i) ,8));
            }

            System.out.print('\u2502');
        }

        nextLine();

        horizontalLineAtBottom();



    }

    public static void horizontalLineAtTop()
    {
        for (int i = 0 ; i < 40 ; i++) // horizontal line
        {
            if (i == 0)
            {
                System.out.print('\u250c');
            }
            else if (i == 39)
            {
                System.out.print('\u2510');
            }
            else
            {
                System.out.print('\u2500');
            }

        }
    }

    public static void nextLine()
    {
        System.out.println("");
    }

    public static void horizontalLineAtMiddle()
    {
        for (int i = 0 ; i < 40 ; i++)
        {
            if (i == 0)
            {
                System.out.print('\u251C');
            }
            else if (i == 39)
            {
                System.out.print('\u2524');
            }
            else
            {
                System.out.print('\u2500');
            }
        }
    }

    public static void horizontalLineAtBottom()
    {
        for (int i = 0 ; i < 40 ; i++)
        {
            if (i == 0 )
            {
                System.out.print('\u2514');
            }
            else if (i == 39)
            {
                System.out.print('\u2518');
            }
            else
            {
                System.out.print('\u2500');
            }
        }
    }

    public static void moveFunction(int selectedIndex , List board)
    {
        Scanner input = new Scanner(System.in);

        System.out.println("tile is locked , use WASD to move the tile , note that you can only swap numbers with zero \npress e to deselect tile");

        String moveDirection = "";

        while(true)
        {
            moveDirection = input.nextLine();

            switch (moveDirection)
            {
                case "w" : if (board.get(selectedIndex - 4).equals("0"))
                {
                    board.set(selectedIndex - 4, board.get(selectedIndex)) ;

                    board.set(selectedIndex , "0") ;
                }
                else if(board.get(selectedIndex).equals("0"))
                {
                    board.set(selectedIndex , board.get(selectedIndex - 4));

                    board.set(selectedIndex - 4 , "0");
                }break; // breaks the switch

                case "s" : if (board.get(selectedIndex + 4).equals("0"))
                {
                    board.set(selectedIndex + 4, board.get(selectedIndex)) ;

                    board.set(selectedIndex , "0") ;
                }
                else if (board.get(selectedIndex).equals("0"))
                {
                    board.set(selectedIndex , board.get(selectedIndex + 4));

                    board.set(selectedIndex + 4 , "0");
                }break; // breaks the switch

                case "d" : if (board.get(selectedIndex + 1).equals("0"))
                {
                    board.set(selectedIndex + 1, board.get(selectedIndex)) ;

                    board.set(selectedIndex , "0") ;
                }
                else if (board.get(selectedIndex).equals("0"))
                {
                    board.set(selectedIndex , board.get(selectedIndex + 1));

                    board.set(selectedIndex + 1 , "0");
                }break; // breaks the switch

                case "a" : if (board.get(selectedIndex - 1).equals("0"))
                {
                    board.set(selectedIndex - 1, board.get(selectedIndex)) ;

                    board.set(selectedIndex , "0") ;
                }
                else if (board.get(selectedIndex).equals("0"))
                {
                    board.set(selectedIndex , board.get(selectedIndex - 1));

                    board.set(selectedIndex - 1 , "0");
                }break; // breaks the switch

                case "e" : break;

                default : System.out.println("not a valid entry , try again"); continue;

            }

            break; // breaks the while
        }

    }

    public static boolean isGameOver(List board)
    {
        boolean ans = true;

        List<String> correctBoard = new ArrayList<String>(); // making the correct board

        for (int i = 0 ; i < 16 ; i++) // filling the correct board
        {
            String temp = String.valueOf(i);

            correctBoard.add(temp);
        }

        for (int i = 0 ; i < 16 ; i++)
        {
            if (!board.get(i).equals(correctBoard.get(i)))
            {
                ans = false;
            }
        }

        return ans;
    }

    public static void printEndMessage()
    {
        System.out.println("Congrats! The Game Is Over");
    }
}
